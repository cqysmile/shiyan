import { defineStore } from 'pinia'

const usePaper = defineStore('paper', {
  state: () => {
    return {
      tableData: [{
        name: '超短波测试',
        level: '0',
        time: 120,
        des: '基础题测试',
      }
      ]
    }
  },
  getters: {

  }
})

export default usePaper