import { defineStore } from 'pinia'

const useSubject = defineStore('subject', {
  state: () => {
    return {
      type: 'wb',
      subjectType: 'Radio',
      query: {
        tiku: '',
        type: ''
      },
      wb: [
        {
          name: '超短波通信是（）',
          answer: 'VHF',
          core: 5,
          type: 'Mutiple',
          time: '2023-12-07 14:43:14',
          json: '{"title":"<p>超短波通信是（）</p>","type":"Radio","attribute":{"required":true,"current":["A"],"core":5,"analysis":"无"},"children":[{"title":"<p>VHF</p>","tag":"A","id":"pnny"},{"title":"<p>AM</p>","tag":"B","id":"h1cd"},{"title":"<p>MF</p>","tag":"C","id":"pnn3y"},{"title":"<p>HF</p>","tag":"D","id":"pnn3y"}]}'
        },
        {
          name: '调频谱仪频宽时，若其它设置为自动，下面哪些参数将不会自动调节到合适值。',
          answer: '电平',
          core: 5,
          type: 'Radio',
          time: '2023-12-07 14:43:14',
          json: `{"title":"<p><span style='color: rgb(31, 31, 31); background-color: rgb(255, 255, 255); font-size: 16px;'>调频谱仪频宽时，若其它设置为自动，下面哪些参数将不会自动调节到合适值。</span></p>","type":"Radio","attribute":{"required":true,"current":["C"],"core":5,"analysis":"无"},"children":[{"title":"<p>视频带宽</p>","tag":"A","id":"pnny"},{"title":"<p>分辨带宽</p>","tag":"B","id":"h1cd"},{"title":"<p>电平</p>","tag":"C","id":"pnn3y"},{"title":"<p>扫描时间</p>","tag":"D","id":"pnn3y"}]}`
        },
        {
          name: `微波功率计按照测量原理大致可分为以下两种类型填空题______和______`,
          answer: '吸收式微波功率计、通过式微波功率计',
          core: 2,
          type: 'Blank',
          time: '2023-12-07 14:43:14',
          json: `{\"title\":\"<p>微波功率计按照测量原理大致可分为以下两种类型填空题<span contenteditable class=\\\"blank_\\\"></span>和<span contenteditable class=\\\"blank_\\\"></span></p>\",\"type\":\"Blank\"}`
        },
        {
          name: `为了保证量值的准确、可靠，用于微波测量的仪器都必须经计量检定，根据结果进行标识，才能借出使用。`,
          answer: '正确',
          core: 2,
          type: 'Boolean',
          time: '2023-12-07 14:43:14',
          json: `{\"title\":\"<p>为了保证量值的准确、可靠，用于微波测量的仪器都必须经计量检定，根据结果进行标识，才能借出使用。</p>\",\"type\":\"Boolean\",\"attribute\":{\"required\":true,\"current\":[\"A\"],\"core\":5,\"analysis\":\"无\"},\"children\":[{\"title\":\"<p>正确</p>\",\"tag\":\"A\",\"id\":\"pnny\"},{\"title\":\"<p>错误</p>\",\"tag\":\"B\",\"id\":\"h1cd\"}]}`
        }, {
          name: `科目一实操题`,
          answer: '',
          core: 20,
          type: 'Action',
          time: '2023-12-07 14:43:14',
          json: ''
        },
      ],
      cdb: [
        {
          name: '[多选题]超短波通信是（）',
          answer: 'VHF',
          core: 5,
          type: 'Radio',
          time: '2023-12-07 14:43:14',
          json: '{"title":"<p>超短波通信是（）</p>","type":"Mutiple","attribute":{"required":true,"current":["A"],"core":5,"analysis":"无"},"children":[{"title":"<p>VHF</p>","tag":"A","id":"pnny"},{"title":"<p>AM</p>","tag":"B","id":"h1cd"},{"title":"<p>MF</p>","tag":"C","id":"pnn3y"},{"title":"<p>HF</p>","tag":"D","id":"pnn3y"}]}'
        },
      ],
      db: []
    }
  },
  getters: {
    subjectLists(state) {
      return state[state.type]
    },

    tableDataWithQuery(state) {
      let keys = ['wb', 'cdb', 'db']
      let { tiku, type } = state.query
      if (tiku) {
        keys = [tiku]
      }

      let arr = []
      keys.forEach(k => {
        arr.push(...state[k])
      })

      if (type) {
        arr = arr.filter(item => item.type !== type)
      }

      return arr
    }
  }
})

export default useSubject