import { defineStore } from 'pinia'

const useMenu = defineStore('menu', {
  state: () => {
    return {
      routes: []
    }
  },
})

export default useMenu