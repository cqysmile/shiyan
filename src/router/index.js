import { createRouter, createWebHashHistory } from 'vue-router';

export const routes = [
  {
    path: '/',
    hidden: true,
    name: '/',
    redirect: '/tiku-manager/home',
    meta: {
      title: '首页'
    },
  },
  {
    path: '/tiku-manager',
    redirect: '/tiku-manager/home',
    name: 'TikuManager',
    meta: {
      title: '题库管理'
    },
    children: [
      {
        path: 'home',
        name: 'TikuManagerHome',
        component: () => import('@/views/tiku-manager/tiku/index.vue'),
        meta: {
          title: '试题库',
          icon: 'Calendar'
        },
      },
      {
        path: 'recycle',
        name: 'TikuManagerRecycle',
        component: () => import('@/views/tiku-manager/recycle/index.vue'),
        meta: {
          title: '试题回收站',
          icon: 'Delete'
        },
      },
    ]
  },
  {
    path: '/exam-manager',
    redirect: '/exam-manager/home',
    name: 'ExamManager',
    meta: {
      title: '考核管理'
    },
    children: [
      {
        path: 'home',
        name: 'ExamManagerHome',
        component: () => import('@/views/exam-manager/index.vue'),
        meta: {
          title: '我的考试',
          icon: 'Calendar'
        },
      },
      {
        path: 'shijuan',
        name: 'ShiJuan',
        component: () => import('@/views/tiku-manager/shijuan/index.vue'),
        meta: {
          title: '试卷管理',
          icon: 'List'
        },
      },
      {
        path: 'shijuan-recycle',
        name: 'ShiJuanRecycle',
        component: () => import('@/views/exam-manager/recycle/index.vue'),
        meta: {
          title: '试卷回收站',
          icon: 'Delete'
        },
      },
      {
        path: 'exame-plan',
        name: 'ExamePlan',
        component: () => import('@/views/tiku-manager/exame-plan/index.vue'),
        meta: {
          title: '考试计划',
          icon: 'Help'
        },
      },
    ]
  },
  {
    path: '/train-manager',
    redirect: '/train-manager/plan',
    name: 'TrainManager',
    meta: {
      title: '培训管理'
    },
    children: [
      {
        path: 'plan',
        name: 'Plan',
        component: () => import('@/views/train-manager/plan/index.vue'),
        meta: {
          title: '教案管理',
          icon: 'Calendar'
        },
      },
      {
        path: 'design',
        name: 'Design',
        component: () => import('@/views/train-manager/design/index.vue'),
        meta: {
          title: '教案设计',
          icon: 'CopyDocument'
        },
      },
      {
        path: 'recycle',
        name: 'PlanRecycle',
        component: () => import('@/views/train-manager/recycle/index.vue'),
        meta: {
          title: '教案回收站',
          icon: 'Delete'
        },
      },
      {
        path: 'message',
        name: 'TrainMessage',
        component: () => import('@/views/train-manager/message/index.vue'),
        meta: {
          title: '培训信息管理',
          icon: 'List'
        }
      }
    ]
  },
  {
    path: '/learn-manager',
    redirect: '/learn-manager/my-learn',
    name: 'LearnManager',
    meta: {
      title: '培训学习'
    },
    children: [
      {
        path: 'my-learn',
        name: 'MyLearn',
        component: () => import('@/views/learn-manager/my-learn/index.vue'),
        meta: {
          title: '我的培训',
          icon: 'Calendar'
        },
      },
    ]
  },
  {
    path: '/property-manager',
    redirect: '/property-manager/laboratory-manager',
    name: 'propertyManager',
    meta: {
      title: '实验资产管理'
    },
    children: [
      {
        path: 'laboratory-manager',
        name: 'laboratoryManager',
        component: () => import('@/views/property-manager/laboratory-manager/index.vue'),
        meta: {
          title: '实验室管理',
          icon: 'OfficeBuilding'
        },
      },
      {
        path: 'equipmentModel-manager',
        name: 'equipmentModelManager',
        component: () => import('@/views/property-manager/equipmentModel-manager/index.vue'),
        meta: {
          title: '设备型号资料库管理',
          icon: 'Platform'
        },
      },
      {
        path: 'deviceProperty-manager',
        name: 'devicePropertyManager',
        component: () => import('@/views/property-manager/deviceProperty-manager/index.vue'),
        meta: {
          title: '设备资产管理',
          icon: 'Suitcase'
        },
      },
      {
        path: 'terminal-manager',
        name: 'terminalManager',
        component: () => import('@/views/property-manager/terminal-manager/index.vue'),
        meta: {
          title: '终端管理',
          icon: 'MessageBox'
        },
      },
      {
        path: 'facilities-manager',
        name: 'facilitiesManager',
        component: () => import('@/views/property-manager/facilities-manager/index.vue'),
        meta: {
          title: '设施管理',
          icon: 'PictureRounded'
        },
      },
    ]
  },
  {
    path: '/system-monitor',
    redirect: '/system-monitor/system-monitorIndex',
    name: 'SystemMonitor',
    meta: {
      title: '实验系统监控'
    },
    children: [
      {
        path: 'system-monitorIndex',
        name: 'system-monitorIndex',
        component: () => import('@/views/system-monitor/system-monitorIndex/index.vue'),
        meta: {
          title: '实验系统监控',
          icon: 'School'
        },
      },
      {
        path: 'terminal-monitorIndex',
        name: 'terminal-monitorIndex',
        component: () => import('@/views/system-monitor/terminal-monitorIndex/index.vue'),
        meta: {
          title: '终端台位监控',
          icon: 'Platform'
        },
      },
    ]
  },
  {
    path: '/equipmentAsset-manager',
    redirect: '/equipmentAsset-manager/equipmentResourcePooling',
    name: 'equipmentAssetManager',
    meta: {
      title: '实验资源管理'
    },
    children: [
      {
        path: 'equipmentResourcePooling',
        name: 'equipmentResourcePooling',
        component: () => import('@/views/equipmentAsset-manager/equipmentResourcePooling/index.vue'),
        meta: {
          title: '实验设备资源池',
          icon: 'School'
        },
      },
      {
        path: 'empiricalData',
        name: 'empiricalData',
        component: () => import('@/views/equipmentAsset-manager/empiricalData/index.vue'),
        meta: {
          title: '实验数据管理',
          icon: 'Histogram'
        },
      },
    ]
  },
  {
    path: '/system-manager',
    redirect: '/system-manager/organization-manager',
    name: 'SystemManager',
    meta: {
      title: '系统管理'
    },
    children: [
      {
        path: 'organization-manager',
        name: 'organizationManager',
        component: () => import('@/views/system-manager/organization-manager/index.vue'),
        meta: {
          title: '组织机构管理',
          icon: 'School'
        },
      },
      {
        path: 'log-manager',
        name: 'logManager',
        component: () => import('@/views/system-manager/log-manager/index.vue'),
        meta: {
          title: '系统日志管理',
          icon: 'Notification'
        },
      },
      {
        path: 'parameter-manager',
        name: 'parameterManager',
        component: () => import('@/views/system-manager/parameter-manager/index.vue'),
        meta: {
          title: '系统参数管理',
          icon: 'Discount'
        },
      },
    ]
  },
]

const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

export default router