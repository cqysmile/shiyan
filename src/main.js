import { createApp } from 'vue'
import './style.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import '@/styles/index.scss'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from '@/router/index.js'

// import DataV, { setClassNamePrefix } from '@dataview/datav-vue3';

let app = createApp(App).use(ElementPlus).use(router).use(createPinia());
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// app.use(DataV, { classNamePrefix: 'dv-' });

app.mount('#app');
