import { Boot } from '@wangeditor/editor'

class MenuBlank {                       // JS 语法

  constructor() {
    this.title = '填空标识'
    // this.iconSvg = '<svg >...</svg>'
    this.tag = 'button'
    this.showModal = true
    this.modalWidth = 300
  }

  // 菜单是否需要激活（如选中加粗文本，“加粗”菜单会激活），用不到则返回 false
  isActive(editor) {                      // JS 语法
    return false
  }

  // 获取菜单执行时的 value ，用不到则返回空 字符串或 false
  getValue(editor) {                               // JS 语法
    return ''
  }

  // 菜单是否需要禁用（如选中 H1 ，“引用”菜单被禁用），用不到则返回 false
  isDisabled(editor) {                     // JS 语法
    return false
  }

  // 点击菜单时触发的函数
  exec(editor, value) {                              // JS 语法
    // Modal menu ，这个函数不用写，空着即可
  }

  // 弹出框 modal 的定位：1. 返回某一个 SlateNode； 2. 返回 null （根据当前选区自动定位）
  getModalPositionNode(editor) {                             // JS 语法
    return null // modal 依据选区定位
  }

  // 定义 modal 内部的 DOM Element
  getModalContentElem(editor) {                        // JS 语法
    editor.insertText('${BLANK}')
    return
  }
}

const menu1Conf = {
  key: 'blank', // 定义 menu key ：要保证唯一、不重复（重要）
  factory() {
    return new MenuBlank() // 把 `YourMenuClass` 替换为你菜单的 class
  },
}

Boot.registerMenu(menu1Conf)