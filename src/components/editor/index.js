import { createVNode, render } from 'vue'
import './menu-blank.js'
import Editor from './index.vue'

export default (event, value, callback, ele) => {
  // 1. 导入组件
  // 2. 根据组件创建虚拟节点
  const el = ele || event.target
  let { width, height } = el.getBoundingClientRect()
  let { transform } = window.getComputedStyle(el)
  if (transform === 'none') {
    el.style.transform = 'translateX(0px)'
  }
  let vnode = createVNode(Editor, { width, height, value, callback })
  // 3. 准备一个DOM容器
  // 4. 把虚拟节点渲染挂载到DOM容器中
  let box = document.createElement('div')

  render(vnode, box)
  el.appendChild(box)
  vnode.component.$destory = () => {
    el.removeChild(box)
    vnode = null
    box = null
  }
}